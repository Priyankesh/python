
x = int(input("Enter value for angle x: "))
y = int(input("Enter value for angle y: "))
z = int(input("Enter value for angle z: "))

if x == y == z:
	print("Equilateral triangle")
elif (x*x)+(y*y)==(z*z) or (z*z+y*y==x*x) or (x*x+z*z==y*y):
	print("Right angle triangle")
elif x==y or y==z or z==x:
	print("isosceles triangle")
